﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace App.Web.Mvc
{
    public static class BundleHtmlHelpers
    {
        private const string BundleConfigFilename = "bundleconfig.json";

        private static List<Bundle> Bundles = GetBundles(HttpContext.Current.Server.MapPath(String.Format("~/{0}", BundleConfigFilename))).ToList();

        public static MvcHtmlString Bundle(this HtmlHelper helper, string path)
        {
            var fileExtension = (Path.GetExtension(path) ?? String.Empty).ToLower();

            var result = String.Empty;

            // If not debugging, render output path as supplied
            if (!HttpContext.Current.IsDebuggingEnabled)
            {    
                switch (fileExtension)
                {
                    case ".js": {
                        result = RenderScriptTag(path);
                        break;
                    }
                    case ".css":
                    {
                        result = RenderLinkTag(path);
                        break;
                    }
                }
                return new MvcHtmlString(result);
            }

            // Else if debugging render bundled all input file paths
            
            var bundle = Bundles.FirstOrDefault(x => x.OutputFileName.Contains((path.StartsWith("/") ? path.Substring(1) : path).Replace(".min" + fileExtension, fileExtension)));

            if (bundle != null)
            {
                foreach (var inputFile in bundle.InputFiles)
                {
                    switch (fileExtension)
                    {
                        case ".js":
                        {
                            result += RenderScriptTag("/" + inputFile);
                            break;
                        }
                        case ".css":
                        {
                            result += RenderLinkTag("/" + inputFile);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
            }

            return new MvcHtmlString(result);
        }

        private static string RenderScriptTag(string path)
        {
            return String.Format(@"<script type=""text/javascript"" src=""{0}""></script>", path);
        }

        private static string RenderLinkTag(string path)
        {
            return String.Format(@"<link href=""{0}"" rel=""stylesheet"" type=""text\css"" />", path);
        }

        private static IEnumerable<Bundle> GetBundles(string configFile)
        {
            if (string.IsNullOrEmpty(configFile))
                return Enumerable.Empty<Bundle>();

            FileInfo file = new FileInfo(configFile);

            if (!file.Exists)
                return Enumerable.Empty<Bundle>();

            string content = File.ReadAllText(configFile);
            var bundles = JsonConvert.DeserializeObject<IEnumerable<Bundle>>(content);

            foreach (Bundle bundle in bundles)
            {
                bundle.FileName = configFile;
            }

            return bundles;
        }
    }

    public class Bundle
    {
        [JsonIgnore]
        public string FileName { get; set; }

        [JsonProperty("outputFileName")]
        public string OutputFileName { get; set; }

        [JsonProperty("inputFiles")]
        public List<string> InputFiles { get; } = new List<string>();

        [JsonProperty("minify")]
        public Dictionary<string, object> Minify { get; } = new Dictionary<string, object> { { "enabled", true } };

        [JsonProperty("includeInProject")]
        public bool IncludeInProject { get; set; } = true;

        [JsonProperty("sourceMap")]
        public bool SourceMap { get; set; }

        internal string Output { get; set; }
    }
}
